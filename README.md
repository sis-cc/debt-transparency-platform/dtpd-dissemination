# DTPD- Data dissemination

The data dissemination concerns the Platform interfaces for data users to consume the data that has been released as public.    
The Data User should be able to:   
- search for relevant data (aggregate level, with drill-down to individual loans feature (to be confirmed during Alpha phase after discussion with the IIF)) and then download them   
- access various predefined dashboards.   
   - features are expected to be available through .Stat Suite ‘Data Explorer’   
   - features through the Power BI dashboarding tool.   
- retrieve data through bulk downloads or machine-to-machine queries in standard SDMX format (this will be the case especially for the Joint External Debt Hub, JEDH).   
